// vim: set syntax=cpp:

#include "analyze.hpp"

#include <cstdint>
#include <numeric>
#include <vector>

#include <cassert>
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <gmpxx.h>

using mpq = mpq_class;
using mpq_vec = std::vector<mpq>;
using dbl_vec = std::vector<mpq>;

template <typename Func>
std::pair<mpq, mpq> probability_dp(Func restart, int N, int K,
                                                    mpq p)
{
    /*
     * This uses the "natural" ordering of n = #throws performed
     *                                     k = #heads gotten
     */
    // Returns true if not to restart
    auto play = [&](int n, int k) -> bool {
        return n == 0 || !restart(N - n, K - k);
    };

    // Note that this is mirrored wrt A!!!
    mpq_vec DP(K + 1, 0);
    mpq_vec DPnew(K + 1, 0);
    DP[0] = 1;

    // Unnormalized average distance
    mpq sc_avgdist = 0;

    // TODO make this single buffer?
    mpq lost = 0;
    for (int n = 0; n < N; ++n) {
        for (int k = 0; k <= K; ++k) {
            if (play(n, k)) {
                if (k == K) {
                    // Aggregate everything with k >= K
                    DPnew[k] += DP[k];
                } else {
                    DPnew[k] += DP[k]*(1 - p); // Tail
                    DPnew[k + 1] += DP[k]*p;       // Head
                }
            } else {
                sc_avgdist += DP[k]*n;
                lost += DP[k];
            }
        }

        auto sum = std::accumulate(DPnew.begin(), DPnew.end(), mpq(0));
        assert(sum+lost == 1); // Sanity check, does remaining and restarted sum to 1?

        swap(DPnew, DP);
        fill(DPnew.begin(), DPnew.end(), 0);
    }

    for (int k = 0; k < K; ++k) {
        sc_avgdist += DP[k]*N;
    }

    mpq &prob_not_restart = DP[K];
    mpq avgdist = sc_avgdist/(1 - prob_not_restart);

    return {prob_not_restart, avgdist};
}

template <typename Func>
std::pair<mpq, mpq> evaluate(Func restart, int N, int K, mpq p)
{
    auto [prob_not_restart, T_cond_restart] = probability_dp(restart, N, K, p);

    assert(prob_not_restart.get_d() != 0); // Any reasonable strategy has a chance to finish

    return {prob_not_restart, T_cond_restart};
}


template <typename Func>
double analyze(Func restart, int N, int K, mpq p)
{
    auto no_restart = []([[maybe_unused]] int n, [[maybe_unused]] int k) {
        return false;
    };

    auto [prob_not_restart, T_cond_restart] = evaluate(restart, N, K, p);
    auto [prob_win, _] = evaluate(no_restart, N, K, p);

    mpq score = T_cond_restart*(1/prob_not_restart - 1) + N;
    mpq prob_not_restart_cond_W = prob_not_restart/prob_win;
    mpq ratio = T_cond_restart/prob_not_restart_cond_W;

    fmt::print("Parameters n k p: {} {} {:.20g}\n", N, K, p.get_d());
    fmt::print("You've done it, g: {:.20g}\n", score.get_d());
    fmt::print("E(T|R): {:.20g}\n",         T_cond_restart.get_d());
    fmt::print("P(!R|W): {:.20g}\n",         prob_not_restart_cond_W.get_d());
    fmt::print("Ratio: {:.20g}\n",           ratio.get_d());

    // print the boundary of the restart region
    for (int n = 0; n <= N; ++n) {
        int k;
        for (k = 0; k <= K; ++k) {
            if (!restart(N - n, K - k)) {
                break;
            }
        }
        std::cout << k << " ";
    }
    std::cout << "\n";

    return score.get_d();
}

//////////////////////////////////////////////////


template <typename Func>
std::vector<std::vector<bool>> prob_strat(Func restart_predicate, int N, int K, mpq p)
{
    /*
     * This uses the "natural" ordering of n = #throws performed
     *                                     k = #heads gotten
     */

    // Note that this is mirrored wrt A!!!
    mpq_vec DP(K + 1, 0);
    DP[K] = 1; // k > K implicitly winning states

    // First pass to get origin value
    for (int n = N-1; n >= 0; --n) {
        for (int k = 0; k <= K; ++k) {
            auto head = k < K ? DP[k + 1] : 1;
            auto tail = DP[k];
            DP[k] = p*head + (1 - p)*tail;
        }
    }
    auto origin = DP[0];

    // Second pass to apply predicate
    fill(DP.begin(), DP.end(), 0);
    DP[K] = 1; // k > K implicitly winning states

    std::vector<std::vector<bool>> restart(N + 1, std::vector<bool>(K + 1, 0));
    for (int n = N-1; n >= 0; --n) {
        for (int k = 0; k <= K; ++k) {
            auto head = k < K ? DP[k + 1] : 1;
            auto tail = DP[k];
            DP[k] = p*head + (1 - p)*tail;

            restart[n][k] = restart_predicate(origin, DP[k]);
        }
    }

    return restart;
}

template <typename Func>
double analyze_prob(Func restart_predicate, int N, int K, mpq p)
{
    auto strat = prob_strat(restart_predicate, N, K, p);
    auto restart = [&](int n, int k) {
        // TODO(alces): The varying standards is infuriating.
        return strat[N - n][K - k];
    };

    return analyze(restart, N, K, p);
}
