#include "compute.hpp"
#include "itertools.hpp"

#include <cassert>
#include <cstdint>
#include <vector>

#include <iostream>
#include <fmt/format.h>
#include <fmt/ostream.h>
#include <gmpxx.h>

using mpq = mpq_class;
using mpq_vec = std::vector<mpq>;
using strat = std::vector<uint8_t>;

template <typename T>
constexpr T relerr (T a, T b)
{
    return abs(a - b)/b;
}

mpq smalleps = 1e-6;

// TODO: rename
mpq dpify(int const N, int const K, mpq const &g, mpq const &p, strat &restart_at)
{
    mpq_vec DP(K + 1, -1);

    for (int k = 0; k <= K; ++k) {
        DP[k] = k <= 0 ? 0 : g;
        restart_at[(N + 1)*k] = true; // TODO was this an error?
    }

    for (int n = 1; n <= N; ++n) {
        for (int k = K; k >= 0; --k) {
            mpq tmp;
            if (k == 0) {
                tmp = n;
            } else {
                tmp = 1 + p*DP[k - 1] + (1 - p)*DP[k];
            }

            DP[k] = std::min(tmp, g);

            restart_at[n + (N + 1)*k] = tmp < g;
        }
    }

    return DP[K];
}

double solve(strat &restart_at, int n, int k, double p)
{
    auto sim = [&](int n, int k, mpq r, mpq p) {
        return dpify(n, k, r, p, restart_at);
    };

    mpq res;

    // Square until  lo <= g < hi
    mpz_class lo = 0, hi = 2;
    mpq_vec pows_lt_hi;
    while ((res = sim(n, k, hi, p)) == hi) {
        lo = hi;
        pows_lt_hi.push_back(hi);
        hi *= hi;
        fmt::print(stderr, "square -> {}\n", hi);
        std::cerr << std::flush;
    }

    // Adjust with the squares where possible
    for (auto &pw : reversed(pows_lt_hi)) {
        fmt::print(stderr, "fiddle with {}\n", pw);
        std::cerr << std::flush;

        // TODO(algmyr): explain this
        hi = lo*pw;
        if ((res = sim(n, k, hi, p)) == hi) {
            lo = hi;
            hi *= pw;
        }
    }

    // Binary search until error is low enough
    mpq l{lo}, mid, r{hi};
    while (relerr(l, r) > smalleps) {
        mid = (r + l)/2;
        fmt::print(stderr, "range [{} {}]\n", l.get_d(), r.get_d());

        res = sim(n, k, mid, p);

        if (res == mid) {
            l = mid;
        } else {
            r = mid;
        }
    }

    // Final update to restart_at using high bound
    sim(n, k, r, p);

    return r.get_d();
}

