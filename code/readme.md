# Source code

The source code for the simulation program, written in C++17.

Prerequisites

* SDL2
* Cairo (cairomm)
* fmtlib
* gmp (and gmpxx)

Compile using

    meson build
    ninja -C build

The program is a simple text interface. Typing `help` gives instruction. Example session:

    k 120
    optimal
    analyze optimal
    analyze promising
    analyze indolent
    export optimal.ppm
