#include "SDL.hpp"

SDL::SDL(int width, int height)
{
    SDL_Init(SDL_INIT_EVERYTHING);
    atexit(SDL_Quit);

    window = SDL_CreateWindow("SDL2", SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED, width, height,
                              SDL_WINDOW_SHOWN);

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    recreateTextures(width, height);
}

void SDL::setSize(int width, int height)
{
    SDL_SetWindowSize(window, width, height);
    recreateTextures(width, height);
}

void
SDL::recreateTextures(int width, int height)
{
    if (texture != nullptr) SDL_DestroyTexture(texture);
    if (textureBG != nullptr) SDL_DestroyTexture(textureBG);

    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888,
                                SDL_TEXTUREACCESS_STREAMING, width, height);
    SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);

    textureBG = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888,
                                  SDL_TEXTUREACCESS_STREAMING, width, height);
    SDL_SetTextureBlendMode(textureBG, SDL_BLENDMODE_BLEND);
}

std::tuple<int,int>
SDL::getSize()
{
    int width,height;
    SDL_GetWindowSize(window, &width, &height);
    return {width, height};
}

std::tuple<int,int>
SDL::getTextureSize(SDL_Texture *tex)
{
    uint32_t format;
    int access, w, h;
    SDL_QueryTexture(tex, &format, &access, &w, &h);
    return {w, h};
}

SDL::~SDL()
{
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}
