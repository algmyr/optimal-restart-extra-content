// vim: set fdm=syntax:

#include <unistd.h>
#include <atomic>
#include <cstdint>
#include <exception>
#include <fstream>
#include <mutex>
#include <sstream>
#include <thread>
#include <tuple>
#include <vector>

#include <SDL2/SDL.h>
#include <cairomm/context.h>
#include <cairomm/surface.h>
#include <cairommconfig.h>

#include <fmt/format.h>

#include "Color.hpp"
#include "compute.hpp"
#include "analyze.hpp"

#define USE_SDL

#ifdef USE_SDL
#include "SDL.hpp"
#else
struct SDL
{
    SDL_Window *window{nullptr};
    SDL_Renderer *renderer{nullptr};
    SDL_Texture *texture{nullptr};
    SDL_Texture *textureBG{nullptr};
    bool running;

    SDL(int width, int height) {
        (void) width;
        (void) height;
    }
    void setSize([[maybe_unused]] int width, [[maybe_unused]] int height) {}
    void recreateTextures([[maybe_unused]] int width, [[maybe_unused]] int height) {}
    std::tuple<int,int> getSize() { return {-1,-1}; }
    std::tuple<int,int> getTextureSize([[maybe_unused]] SDL_Texture *tex) { return {-1,-1}; }
};
#endif

using std::lock_guard;
using std::mutex;
using std::string;
using std::tuple;

struct State
{
    // Could be atomic, but does it really matter here?
    bool running{true};
    bool fliph{false};
    bool flipv{false};
    bool drawOverlay{false};
    bool drawTransform{false};

    int step{50};

    int n;
    int k;
    int width;
    int height;
    double p{.5};
    std::vector<uint8_t> A;

    State(int _n, int _k)
        : n(_n), k(_k), width(_n + 1), height(_k + 1), A(width*height)
    {
    }

    void update(int _n, int _k, double _p)
    {
        n = _n;
        k = _k;
        p = _p;
        width = n + 1;
        height = k + 1;
        A.resize(4*width*height);
    }

    bool restart(int _n, int _k)
    {
        if (_k < 0) return false;
        return A[(n + 1)*_k + _n] == 0;
    }
};

struct texture_lock {
    uint8_t *pixels{nullptr};
    int stride{0};
    SDL_Texture *texture;

    explicit texture_lock(SDL_Texture *tex) : texture(tex)
    {
        void *data;
        SDL_LockTexture(texture, nullptr, &data, &stride);
        pixels = static_cast<uint8_t *>(data);
    }
    texture_lock(texture_lock const &lock) = default;
    texture_lock(texture_lock &&lock) = default;

    texture_lock &operator=(texture_lock &&) = default;
    texture_lock &operator=(texture_lock const &) = default;

    ~texture_lock() { SDL_UnlockTexture(texture); }
};

//////////////////////////////////////////

mutex mtx;

/*
 * Graphics and input
 */
void handleInput(State &state)
{
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        if (SDL_QUIT == event.type) {
            state.running = false;
            break;
        }

        if (SDL_KEYDOWN == event.type) {
            switch (event.key.keysym.scancode) {
            case SDL_SCANCODE_H:
                state.fliph = !state.fliph;
                cprint<blue>("horizontal flip\n");
                break;
            case SDL_SCANCODE_V:
                state.flipv = !state.flipv;
                cprint<blue>("vertical flip\n");
                break;
            case SDL_SCANCODE_G:
                state.drawOverlay = !state.drawOverlay;
                cprint<blue>("overlay toggled\n");
                break;
            case SDL_SCANCODE_S:
                state.step += 10;
                cprint<blue>("step changed to {}\n", state.step);
                break;
            case SDL_SCANCODE_A:
                if (state.step - 10 > 0) state.step -= 10;
                cprint<blue>("step changed to {}\n", state.step);
                break;
            case SDL_SCANCODE_T:
                state.drawTransform = !state.drawTransform;
                cprint<blue>("transform toggled\n");
            default:
                break;
            }
        }
    }
}

void update([[maybe_unused]] State &state) {}

void renderBG(SDL &sdl, State &state)
{
    texture_lock tex_lock(sdl.textureBG);

    auto &width = state.width;
    auto &height = state.height;

    uint8_t *pixels = tex_lock.pixels;
    auto *_dst = reinterpret_cast<uint32_t *>(pixels);
    auto *_src = reinterpret_cast<uint8_t *>(state.A.data());

    auto IX = [&](int i, int j) { return i*width + j; };
    auto src = [&](int x, int y) -> auto & { return _src[IX(y, x)]; };
    auto dst = [&](int x, int y) -> auto & { return _dst[IX(y, x)]; };

    uint8_t const NO_RESTART = 200;
    uint8_t const RESTART = 100;

    auto gray = [](uint8_t x) -> uint32_t {
        return (255 << 24) | (x << 16) | (x << 8) | x;
    };

    if (state.drawTransform) {
        auto val = [&](int x, int y) -> uint8_t {
            int tosses = state.n - x;
            int heads = state.k - y;
            if (tosses < 0 or tosses >= width or heads < 0 or heads >= tosses) {
                return 0;
            }
            if (heads >= height) {
                return RESTART;
            }
            return src(x, y) ? RESTART : NO_RESTART;
        };

        auto pixel_value = [&](int y, int x) -> uint32_t {
            double src_y = y - (state.n - x)*state.p + state.k/2.0;
            int src_y_trunc = static_cast<int>(src_y);
            double alpha = src_y - src_y_trunc;

            double mix = (1 - alpha)*val(x, src_y_trunc) +
                         alpha*val(x, src_y_trunc + 1);
            return gray(static_cast<uint8_t>(mix));
        };

        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                dst(state.fliph ? width - x - 1 : x,
                    state.flipv ? height - y - 1 : y) = pixel_value(y, x);
            }
        }
    } else {
        auto pixel_value = [&](int y, int x) {
            return gray(src(x, y) ? RESTART : NO_RESTART);
        };

        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                dst(state.fliph ? width - x - 1 : x,
                    state.flipv ? height - y - 1 : y) = pixel_value(y, x);
            }
        }
    }
}

void renderFG(SDL &sdl, State &state)
{
    texture_lock tex_lock(sdl.texture);
    void *pixels = tex_lock.pixels;
    int stride = tex_lock.stride;

    // Cairo
    auto [width, height] = sdl.getTextureSize(sdl.texture);
    memset(pixels, 0, width*height*4);

    if (!state.drawOverlay) return;

    auto surface = Cairo::ImageSurface::create(
        (uint8_t *)pixels, Cairo::Format::FORMAT_ARGB32, width, height, stride);
    auto cr = Cairo::Context::create(surface);

    // Clear to white
    cr->save();
    cr->set_source_rgba(1, 1, 1, 0);
    cr->paint();
    cr->restore();

    // Make transform
    double w = width, h = height;
    if (state.flipv && state.fliph) {
        cr->transform((Cairo::Matrix){-1, 0, 0, -1, w, h});
    } else if (state.flipv && !state.fliph) {
        cr->transform((Cairo::Matrix){+1, 0, 0, -1, 0, h});
    } else if (!state.flipv && state.fliph) {
        cr->transform((Cairo::Matrix){-1, 0, 0, +1, w, 0});
    }

    // Draw grid
    cr->set_source_rgba(0, 0, 0, 0.3);
    int step = state.step;
    for (int x = step; x < width; x += step) {
        cr->move_to(x, 0);
        cr->line_to(x, height);
    }
    for (int y = step; y < height; y += step) {
        cr->move_to(0, y);
        cr->line_to(width, y);
    }

    // Draw important lines
    cr->move_to(0, 0);                      // Winning pace
    cr->line_to(width, height);
    cr->move_to(width, height);             // Expected pace
    cr->line_to(0, height - width*state.p);
    cr->move_to(width, height);             // Best possible
    cr->line_to(width - height, 0);
    cr->move_to(0, 0);                      // Guaranteed loss
    cr->line_to(height, height);

    cr->stroke();
}

void render(SDL &sdl, State &state)
{
    // Clear
    SDL_SetRenderDrawColor(sdl.renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(sdl.renderer);

    renderBG(sdl, state);
    SDL_RenderCopy(sdl.renderer, sdl.textureBG, NULL, NULL);

    renderFG(sdl, state);
    SDL_RenderCopy(sdl.renderer, sdl.texture, NULL, NULL);
    
    // Output
    SDL_RenderPresent(sdl.renderer);
}



/*
 * Actual logic here
 */

// This is actually pretty neat! :)
// Now linear!

template <typename... args>
struct parser {
    tuple<args...> value;

    template<int i>
    void _parse([[maybe_unused]] std::istream &ss) {}

    template<int i, typename T, typename ...rest>
    void _parse(std::istream &ss) {
        ss >> std::get<i>(value);
        if (ss.fail()) {
            throw std::invalid_argument("Invalid arguments.");
        } else {
            _parse<i+1,rest...>(ss);
        }
    }

    tuple<args...> operator ()(std::istream &ss) {
        _parse<0,args...>(ss);
        return value;
    }
};

void exportPPM(SDL &sdl, State &state, string fname)
{
    texture_lock tex_lock(sdl.texture);
    texture_lock tex_lock_bg(sdl.textureBG);
    lock_guard<mutex> pixel_lock(mtx);

    uint8_t *fg = tex_lock.pixels;
    uint8_t *bg = tex_lock_bg.pixels;
    int n = state.width;
    int k = state.height;

    auto combine = [](uint8_t *bg, uint8_t *fg) {
        float B = bg[0]/255.f;
        float G = bg[1]/255.f;
        float R = bg[2]/255.f;
        float A = bg[3]/255.f;
        B *= A;
        G *= A;
        R *= A;

        float b = fg[0]/255.f;
        float g = fg[1]/255.f;
        float r = fg[2]/255.f;
        float a = fg[3]/255.f;
        b *= a;
        g *= a;
        r *= a;

        auto r_out = static_cast<uint8_t>(255*(r + (1 - a)*R));
        auto g_out = static_cast<uint8_t>(255*(g + (1 - a)*G));
        auto b_out = static_cast<uint8_t>(255*(b + (1 - a)*B));
        auto a_out = static_cast<uint8_t>(255*(a + (1 - a)*A));

        return std::make_tuple(r_out, g_out, b_out, a_out);
    };

    // Binary PPM
    std::ofstream fs(fname);
    fs << "P6\n";
    fs << n << " " << k << '\n';
    fs << 255 << '\n';
    for (int i = 0; i < k; ++i) {
        string sep = "";
        for (int j = 0; j < n; ++j) {
            int ix = 4*(i*n + j);
            auto [r,g,b,a] = combine(bg+ix, fg+ix);
            fs << r << g << b; // BGRA
        }
    }
}

/*
 * Command line parser
 */

double optiscore = NAN;
void handle_line(SDL &sdl, State &state, std::stringstream &ss)
{
    string cmd; ss >> cmd;

    if (cmd == "quit") {
        state.running = false;
    } else if (cmd == "n") {
        auto [n] = parser<int>()(ss);
        {
            lock_guard<mutex> pixel_lock(mtx);
            state.update(n,state.k,state.p);
            sdl.setSize(state.width, state.height);
        }
    } else if (cmd == "k") {
        auto [k] = parser<int>()(ss);
        {
            lock_guard<mutex> pixel_lock(mtx);
            state.update(state.n,k,state.p);
            sdl.setSize(state.width, state.height);
        }
    } else if (cmd == "p") {
        auto [p] = parser<double>()(ss);
        {
            lock_guard<mutex> pixel_lock(mtx);
            state.update(state.n,state.k,p);
            sdl.setSize(state.width, state.height);
        }
    } else if (cmd == "params") {
        auto [n,k,p] = parser<int,int,double>()(ss);
        {
            lock_guard<mutex> pixel_lock(mtx);
            state.update(n,k,p);
            sdl.setSize(state.width, state.height);
        }
    } else if (cmd == "optimal") {
        double score = solve(state.A, state.n, state.k, state.p);
        cprint<green>("Optimal g is {}\n", score);
    } else if (cmd == "export") {
        auto [fname] = parser<string>()(ss);

        exportPPM(sdl, state, fname);

        cprint<green>("Wrote ppm to \"{}\"\n", fname);
    } else if (cmd == "analyze") {
        auto [strat] = parser<string>()(ss);
        double score;

        // First arg to analyze is the restart criterion
        if (strat == "optimal") {
            score = analyze([&state](int n, int k){return state.restart(n,k);},
                           state.n, state.k, state.p);
            optiscore = score;
        } else if (strat == "indolent") {
            score = analyze([&state]([[maybe_unused]] int n,
                                     [[maybe_unused]] int k){return false;},
                           state.n, state.k, state.p);
        } else if (strat == "notstupid") {
            score = analyze([&state](int n, int k){
                        return k > n;
                   }, state.n, state.k, state.p);
        } else if (strat == "promising") {
            score = analyze([&state](int n, int k){
                        double needed   = (double)state.k/state.n;
                        double expected = state.p;
                        double eps = needed - expected;

                        if (eps <= 0) return false;
                        
                        //int step = 1;
                        int step = ceil(0.5/(eps*eps));

                        int heads  = state.k - k;
                        int tossed = state.n - n;

                        double current = (double)heads/tossed;

                        if (tossed%step == step - 1)
                            return current < expected + eps/2;
                        return false;
                   }, state.n, state.k, state.p);
        } else if (strat == "probability") {
            score = analyze_prob([](auto &start, auto &cur) {
                        return cur < .5*start;
                    }, state.n, state.k, state.p);
        } else {
            cprint<red>("Error: \"{}\" is not a valid strategy\n", strat);
            return;
        }

        cprint<green>("Compared to optimal: {:.20g}\n", score/optiscore);
        //cprint<green>("Win scoreability is {}\n", score);
    } else if (cmd == "help") {
        cprint<blue>("Available commands are:\n"
                     "  n <n>\n"
                     "  k <k>\n"
                     "  p <p>\n"
                     "  params <n> <k> <p>\n"
                     "  optimal             // compute optimal strat\n"
                     "  export <filename>   // export plot as ppm\n"
                     "  analyze <strat>     // compute info about strat\n"
                     "Strats:\n"
                     "  optimal, indolent, notstupid, promising, probability\n"
        );
    } else {
        cprint<red>("Error: \"{}\" is not a valid command\n", cmd);
    }
}

void cmdline(SDL &sdl, State &state)
{
    bool interactive = isatty(fileno(stdin));

    while(state.running) {
        if (interactive) {
            fmt::print("[n={}, k={}, p={}]> ", state.n, state.k, state.p);
        }

        string line;
        std::getline(std::cin, line);
        std::stringstream ss(line);

        if (not interactive) {
            cprint<blue>("{}\n", line);
        }

        if (std::cin.eof()) {
            state.running = false;
            break;
        }

        try {
            handle_line(sdl, state, ss);
        } catch (std::invalid_argument &e) {
            cprint<red>("Error: {}\n", e.what());
        }
    }
}

int main()
{
    State state(200, 100);
    SDL sdl(state.width, state.height);

    std::thread t1(cmdline, std::ref(sdl), std::ref(state));

#ifdef USE_SDL
    int frameduration = 1000/30;
    int nextFrame = SDL_GetTicks() + frameduration;
    while (state.running)
    {
        {
            lock_guard<mutex> pixel_lock(mtx);
            handleInput(state);
            update(state);
            render(sdl, state);
        }

        int now = SDL_GetTicks();
        if (now < nextFrame) {
            SDL_Delay(nextFrame-now);
        }
        nextFrame += frameduration;
    }

    t1.detach();
#else
    std::cerr << "waiting for shit to join" << std::endl;
    t1.join();
    std::cerr << "done waiting for shit to join" << std::endl;
#endif
}
