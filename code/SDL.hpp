#ifndef MYSDLHEADER
#define MYSDLHEADER

#include <tuple>

#include <SDL2/SDL.h>

struct SDL
{
    SDL_Window *window{nullptr};
    SDL_Renderer *renderer{nullptr};
    SDL_Texture *texture{nullptr};
    SDL_Texture *textureBG{nullptr};
    bool running;

    SDL(int width, int height);

    void setSize(int width, int height);

    void recreateTextures(int width, int height);

    std::tuple<int,int> getSize();

    std::tuple<int,int> getTextureSize(SDL_Texture *tex);

    ~SDL();
};

#endif /* end of include guard */
