#ifndef TERMCOLOR_H
#define TERMCOLOR_H

#include <iostream>
#include <string>
#include <fmt/format.h>
#include <fmt/ostream.h>

enum Color {
    black,
    red,
    green,
    yellow,
    blue,
    purple,
    cyan,
    white,

    black_faint,
    red_faint,
    green_faint,
    yellow_faint,
    blue_faint,
    purple_faint,
    cyan_faint,
    white_faint,

    reset,
};

constexpr std::string_view cstring(Color color) {
    switch (color) {
    case black:        return "\x1b[1;30m";
    case red:          return "\x1b[1;31m";
    case green:        return "\x1b[1;32m";
    case yellow:       return "\x1b[1;33m";
    case blue:         return "\x1b[1;34m";
    case purple:       return "\x1b[1;35m";
    case cyan:         return "\x1b[1;36m";
    case white:        return "\x1b[1;37m";

    case black_faint:  return "\x1b[30m";
    case red_faint:    return "\x1b[31m";
    case green_faint:  return "\x1b[32m";
    case yellow_faint: return "\x1b[33m";
    case blue_faint:   return "\x1b[34m";
    case purple_faint: return "\x1b[35m";
    case cyan_faint:   return "\x1b[36m";
    case white_faint:  return "\x1b[37m";

    case reset:        return "\x1b[0m";

    default:           return "";
    }
}

template <Color color, typename... Args>
void cprint(const char *format, const Args & ... args) {
    std::cout << cstring(color);
    fmt::print(format, args...);
    std::cout << cstring(reset);
    std::cout.flush();
}

#endif /* end of include guard */
