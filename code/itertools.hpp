#ifndef ITERTOOLS
#define ITERTOOLS

template <typename T>
struct reversed {
    explicit reversed(T& container) : container(container) {}
    decltype(auto) begin() { return container.rbegin(); }
    decltype(auto) end() { return container.rend(); }
    T& container;
};

#endif /* end of include guard */
