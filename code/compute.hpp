#ifndef COMPUTE
#define COMPUTE

#include <vector>
#include <cstdint>

double solve(std::vector<uint8_t> &A, int n, int k, double p);

#endif /* end of include guard */
