# Extra content for Optimal Restart thesis

Here you'll find code used in the thesis work, as well as a more comprehensive collection of plots generated with that code.

To more easily view the graphs we recommend to clone (or in some other way download) the repository and view things locally.